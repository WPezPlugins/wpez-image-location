<?php
/*
Plugin Name: WPezPlugins: Image Location (Proof of Concept)
Plugin URI: TODO
Description: A utility plugin for WordPress theme and plugin developers who embrace the importance of the img tag's sizes and srcset attributes.
Version: 0.0.0
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: http://AlchemyUnited.com
License: GPLv2+
Text Domain: wpez-imgloc
Domain Path: /languages
*/

namespace WPezImageLocation;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

use WPezImageLocation\App\ClassPlugin;

$str_php_ver_comp = '5.6.20';

// we reserve the right to use traits :)
if (version_compare(PHP_VERSION, $str_php_ver_comp, '<')) {
    exit(sprintf('This plugin - namespace: ' . __NAMESPACE__ . ' - requires PHP ' . esc_html($str_php_ver_comp) . ' or higher. Your WordPress site is using PHP %s.', PHP_VERSION));
}


function autoloader( $bool = true ){

    if ( $bool !== true ) {
        return;
    }

    require_once 'App/Core/Autoload/ClassWPezAutoload.php';

    $new_autoload = new ClassWPezAutoload();
    $new_autoload->setPathParent( dirname( __FILE__ ) );
    $new_autoload->setNeedle(__NAMESPACE__ );
    $new_autoload->setReplaceSearch(__NAMESPACE__ . DIRECTORY_SEPARATOR);

    spl_autoload_register( [$new_autoload, 'WPezAutoload'], true );
}
autoloader();

function plugin($bool = true){

    if ( $bool !== true ) {
        return;
    }

    $new_plugin = new ClassPlugin();
}
plugin();