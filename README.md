## WPezPlugins: Image Location (Proof of Concept)

__A utility plugin for theme and plugin developers who understand the importance of the img tag's sizes and srcset attributes.__ 


> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Overview

If you're not familiar with img, sizes and srcset then please read this article. 

https://ericportis.com/posts/2014/srcset-sizes/

This is helpful as well:

https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images

If you're already aware of these things then you understand how essential the sizes attribute is to page speed optimization. The srcset helps, but sizes is where the magic happens. Sizes enables the browser to make intelligent and optimal decisions (about which image to use from the srcset). 

The problem is simple: WordPress has the necessary filters, but it does not communicate context (i.e., the location of the image(s) being rendered. 

Yes, you can use conditional tags to generalize your decisions. For example, if you have a blog index listing grid, you'll be able to target those imgs' sizes with a conditional tag. However, if you have a layout where the images are less consistent then conditional tags aren't good enough. You need more information, information WP isn't able to provide.

> --
>
> If you're using Gutenberg then WPezPlugins' GutenBetter Image plugin is a must have.
> https://gitlab.com/WPezPlugins/wpez-guten-better-image
>
> --

### How To

The plugin "listens" for:

**$str_location** - A unique string that identifies a given image or group of images.

**$arr_args** - An array of location-centric args:

- 'content_width' => The current content width
- 'columns'       => If it's a grid, the number of columns
- 'img_padding'   => The total of the img's padding-right and padding-left
- 'img_margin'    => The total of the img's margin-right and margin-left
- 'sizes'         => The "suggested" sizes string,
- 'srcset'        => The "suggested" srcset string,

_Note: None of the args are required. As the theme / plugin dev you can allow those decisions to made downstream._However, you should try to pass sizes and srcset, else the standard WordPress 


    do_action('wpez_image_location_open', $str_location , $arr_args);
    the_post_thumbnail();
    do_action('wpez_image_location_close', $str_location );`

Now, with the necessary intelligence, the plugin can use the filter wp_get_attachment_image_attributes and is able to adjust the sizes and srcset attributes on the fly. 







 


### FAQ

__1 - Why?__

Because the img tag's sizes attribute is very important; and in order to set it correctly, you need to know where a given image is being rendered. 


### Helpful Links

- https://ericportis.com/posts/2014/srcset-sizes/

- https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images

- wp_get_attachment_image_attributes

- https://gitlab.com/WPezPlugins/wpez-guten-better-image
 
 
### TODO

- TBD

### Change Log

__-- 0.0.0__

- INIT - Hey! Ho!! Let's go!!! Proof of Concept. 


