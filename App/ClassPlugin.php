<?php

namespace WPezImageLocation\App;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

use WPezImageLocation\App\Core\HooksRegister\ClassHooksRegister as HooksRegister;
use WPezImageLocation\App\Plugin\Main\ClassImageLocationCustomize as ImgLocCustom;
use WPezImageLocation\App\Plugin\Main\ClassImageLocation as ImgLoc;


class ClassPlugin {

	protected $_new_img_loc;
	protected $_new_hooks_reg;
	protected $_arr_actions;
	protected $_arr_filters;

    public function __construct() {

        $this->setPropertyDefaults();

	    $this->actions(true);

	    $this->filters(true);

	    // this should be last
	    $this->hooksRegister();

    }


    protected function setPropertyDefaults() {

    	$new_img_loc_custom = new ImgLocCustom();
    	$this->_new_img_loc = new ImgLoc($new_img_loc_custom);

	    $this->_new_hooks_reg = new HooksRegister();
	    $this->_arr_actions = [];
	    $this->_arr_filters = [];

    }

	/**
	 * After gathering (below) the arr_actions and arr_filter, it's time to
	 * make some RegisterHook magic
	 */
	protected function hooksRegister()
	{

		$this->_new_hooks_reg->loadActions($this->_arr_actions);

		$this->_new_hooks_reg->loadFilters($this->_arr_filters);

		$this->_new_hooks_reg->doRegister();

	}

	public function actions($bool = true)
	{

		if ($bool !== true) {
			return;
		}

		$this->_arr_actions[] = [
			'active' => true, //
			'hook' => 'wpez_image_location_open',
			'component' => $this,
			'callback' => 'wpezImageLocationOpen',
			'priority' => 10,
			'accepted_args' => 2
		];

		$this->_arr_actions[] = [
			'active' => true, // active does not have to be set, default: true
			'hook' => 'wpez_image_location_close',
			'component' => $this,
			'callback' => 'wpezImageLocationClose',
			'priority' => 10,
			'accepted_args' => 2
		];
	}

	public function wpezImageLocationOpen($str_loc, $arr_args){

		$this->_new_img_loc->setLocation($str_loc);
		$this->_new_img_loc->setLocationArgs($arr_args);
	}

	public function wpezImageLocationClose($str_loc, $arr_args = []){

		$this->_new_img_loc->reset($str_loc);
	}

	public function filters($bool = true) {


		if ( $bool !== true ) {
			return;
		}

		/*
		 Might need these eventually?

		$this->_arr_filters[] = [
			'active' => true,
			'hook' =>   'wp_calculate_image_srcset',
			'component' => $this,
			'callback' => 'TODO',
			'priority' => 1000,
			'accepted_args' => 5
		];

		$this->_arr_filters[] = [
			'active' => true,
			'hook' =>   'wp_calculate_image_sizes',
			'component' => $this,
			'callback' => 'TODO',
			'priority' => 1000,
			'accepted_args' => 5
		];

		*/

		$this->_arr_filters[] = [
			'active' => true,
			'hook' =>   'wp_get_attachment_image_attributes',
			'component' => $this->_new_img_loc,
			'callback' => 'wpGetAttachmentImageAttributes',
			'priority' => 1000,
			'accepted_args' => 3
		];
	}



}