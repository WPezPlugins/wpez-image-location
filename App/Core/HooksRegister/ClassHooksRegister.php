<?php
/**
 * As inspired by:
 *
 * https://code.tutsplus.com/tutorials/object-oriented-programming-in-wordpress-building-the-plugin-ii--cms-21105
 *
 * https://github.com/DevinVinson/WordPress-Plugin-Boilerplate/blob/master/plugin-name/includes/class-plugin-name-loader.php
 *
 * The notable ez improvement is loading is done with arrays, not (harcoded)
 * code. This means, you can define your hooks and then - prior to loading -
 * slap a filter on those arrays to allow for customization by users (read:
 * other devs). As a result any hook'ed callback (method) can be replaced as
 * needed.
 *
 * Yeah. Cool :)
 *
 */

namespace WPezImageLocation\App\Core\HooksRegister;


// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

class ClassHooksRegister{

    // protected $_arr_types;
    protected $_arr_actions;
    protected $_arr_filters;
    protected $_arr_hook_defaults;


    public function __construct() {

        $this->setPropertyDefaults();

    }

    protected function setPropertyDefaults() {

        // $this->_arr_types = ['action', 'filter'];

        $this->_arr_actions = [];
        $this->_arr_filters = [];

        $this->_arr_hook_defaults = [
            //      'type'          => false,
            'active'        => true,
            'hook'          => false,
            'component'     => false,
            'callback'      => false,
            'priority'      => 10,
            'accepted_args' => 1
        ];
    }

    public function pushAction( $arr_args = false){

        return $this->pushMaster('_arr_actions', $arr_args);

    }

    public function pushFilter( $arr_args = false ){

        return $this->pushMaster('_arr_filters', $arr_args);

    }


    protected function pushMaster( $str_prop = false, $arr_args = false ){

        if ( ! is_array($arr_args)){
            return false;
        }

        $obj_hook = (object)array_merge( $this->_arr_hook_defaults, $arr_args );

        // maybe we have a problem?
        if ( $obj_hook->active !== true || $obj_hook->hook === false || $obj_hook->component === false || $obj_hook->callback === false ) {
            return false;
        }

        $this->$str_prop[] = $obj_hook;
        return true;

    }

    public function loadActions( $arr_arrs = [] ) {

        if ( ! is_array( $arr_arrs ) ) {
            return false;
        }

        $this->loadMaster( 'pushAction', $arr_arrs );
        return true;

    }

    public function loadFilters( $arr_arrs = [] ) {

        if ( ! is_array( $arr_arrs ) ) {
            return false;
        }
        $this->loadMaster( 'pushFilter', $arr_arrs );
        return true;
    }


    protected function loadMaster( $str_method, $arr_arrs = [] ) {

        foreach ( $arr_arrs as $arr) {

            $this->$str_method($arr);

        }
    }


    public function doRegister( $bool_actions = true, $bool_filters = true ) {

        if ( $bool_actions === true && ! empty( $this->_arr_actions ) ) {

            $this->register( 'add_action', $this->_arr_actions );
        }

        if ( $bool_filters === true && ! empty( $this->_arr_filters ) ) {

            $this->register( 'add_filter', $this->_arr_filters );
        }

    }

    protected function register( $str_wp_function = '', $arr_objs = [] ) {

        foreach ( $arr_objs as $obj ) {

            $str_wp_function( $obj->hook, [ $obj->component, $obj->callback ], $obj->priority, $obj->accepted_args );
        }
    }

}