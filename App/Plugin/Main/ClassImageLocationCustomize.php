<?php

namespace WPezImageLocation\App\Plugin\Main;

class ClassImageLocationCustomize implements InterfaceImageLocationCustomize {

	protected $_str_filter_prefix;

	public function __construct() {
		$this->setPropertyDefaults();

	}

	protected function setPropertyDefaults() {

		$this->_str_filter_prefix = 'wpez_image_location';

	}


	public function getAttributes($arr_attr_defaults, $str_location, $arr_location_args, $attr, $post, $size){

		$sizes_new = apply_filters( $this->_str_filter_prefix . '_attributes', $arr_attr_defaults, $str_location, $arr_location_args, $attr, $post, $size);

		return $sizes_new;

	}

}