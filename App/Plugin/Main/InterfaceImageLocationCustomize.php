<?php

namespace WPezImageLocation\App\Plugin\Main;

interface InterfaceImageLocationCustomize
{

	public function getAttributes($arr_attr_deaults, $str_location, $arr_location_args, $attr, $post, $size);

}
