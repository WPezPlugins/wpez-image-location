<?php

namespace WPezImageLocation\App\Plugin\Main;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

// use WPezGutenBetterImage\App\Plugin\Blocks\ClassRenderCallback as RegCB;


class ClassImageLocation {

	protected $_new_customize;
	protected $_str_location;
	protected $_arr_location_args;
	protected $_arr_location_args_defaults;
	protected $_arr_attr_defaults;


	public function __construct( InterfaceImageLocationCustomize $objCustomize ) {

		$this->_new_customize = $objCustomize;
		$this->setPropertyDefaults();

	}

	/**
	 *
	 */
	protected function setPropertyDefaults() {
		$this->_str_location      = false;
		$this->_arr_location_args_defaults = [
			'content_width' => false,
			'columns'       => false,
			'img_padding'   => false,
			'img_margin'    => false,
			'sizes'         => false,
			'srcset'        => false
		];
		$this->_arr_location_args = $this->_arr_location_args_defaults;
		$this->_arr_attr_defaults = [
			'src'    => false,
			'class'  => false,
			'alt'    => false,
			'srcset' => false,
			'sizes'  => false
		];
	}

	public function setLocation( $str_location = false ) {
		if ( is_string( $str_location ) ) {

			$this->_str_location = $str_location;
			return true;
		}

		return false;
	}

	public function setLocationArgs( $arr_args = false ) {

		if ( is_array( ( $arr_args ) ) ) {

			$this->_arr_location_args = array_merge( $this->_arr_location_args_defaults, $arr_args );
			return true;
		}

		return false;
	}

	public function reset( $str_location = false  ) {

		// are we closing what's open?
		if ( $str_location === $this->_str_location ) {
			$this->setPropertyDefaults();
		}
	}


	public function wpGetAttachmentImageAttributes( $attr, $post, $size ) {

		$arr_attr_new = $this->_new_customize->getAttributes( $this->_arr_attr_defaults, $this->_str_location, $this->_arr_location_args, $attr, $post, $size );
		// do we have customizations?
		if ( is_array( $arr_attr_new ) ) {
			// remove anything that isn't necessary
			$arr_attr_new = array_diff( $arr_attr_new, [ false ] );
		} else {
			$arr_attr_new = [];
		}

		// merge the three
		$attr = array_merge( $attr, array_diff( $this->_arr_location_args, [ false ] ), $arr_attr_new );

		// TODO - remove keys that WP doesn't need/want

		return $attr;

	}
}








